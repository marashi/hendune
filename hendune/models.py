from django.db import models

# Create your models here.

class Word(models.Model):
	key = models.CharField(max_length=200)
	definition = models.CharField(max_length=13171)
	def __str__(self):
		return self.key
	def __unicode__(self):
		return self.key
